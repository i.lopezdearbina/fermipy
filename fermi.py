# routine dfermi gets the fermi-dirac functions and their derivaties
# routine fdfunc1 forms the integrand of the fermi-dirac functions
# routine fdfunc2 same as fdfunc but with the change of variable z**2=x
# routine dqleg010 does 10 point gauss-legendre integration  9 fig accuracy
# routine dqleg020 does 20 point gauss-legendre integration 14 fig accuracy
# routine dqleg040 does 40 point gauss-legendre integration 18 fig accuracy
# routine dqleg080 does 80 point gauss-legendre integration 32 fig accuracy
# routine dqlag010 does 10 point gauss-laguerre integration  9 fig accuracy
# routine dqlag020 does 20 point gauss-laguerre integration 14 fig accuracy
# routine dqlag040 does 40 point gauss-laguerre integration 18 fig accuracy
# routine dqlag080 does 80 point gauss-laguerre integration 32 fig accuracy
import numpy as np


def  dfermi(dk,eta,theta, calcDerivatives = False):

    """
        this routine computes the fermi-dirac integrals of 
        index dk, with degeneracy parameter eta and relativity parameter theta.
        input is dk the double precision index of the fermi-dirac function,
        eta the degeneracy parameter, and theta the relativity parameter.
        the output is fd is computed by applying three 10-point 
        gauss-legendre and one 10-point gauss-laguerre rules over
        four appropriate subintervals. the derivative with respect to eta is
        output in fdeta, and the derivative with respct to theta is in fdtheta.
        within each subinterval the fd kernel.

        this routine delivers at least 9 figures of accuracy

        eta may be a scalar or a numpy array
        dk and theta may be scalars or numpy arrays of the same shape as eta

        reference: j.m. aparicio, apjs 117, 627 1998
        Converted to Python by Mario Jakobs on 2021-09-17
    """

#   parameters defining the location of the breakpoints for the
#   subintervals of integration:
    d = 3.3609e0
    sg = 9.1186e-2
    a1 = 6.7774e0
    b1 = 1.1418e0
    c1 = 2.9826e0
    a2 = 3.7601e0
    b2 = 9.3719e-2
    c2 = 2.1063e-2
    d2 = 3.1084e1
    e2 = 1.0056e0
    a3 = 7.5669e0
    b3 = 1.1695e0
    c3 = 7.5416e-1
    d3 = 6.6558e0
    e3 = -1.2819e-1


#   integrand parameters:
    etaScalar = not isinstance(eta, np.ndarray)
    dkScalar = not isinstance(dk, np.ndarray)
    thetaScalar = not isinstance(theta, np.ndarray)
    scalar = etaScalar and dkScalar and thetaScalar
    if etaScalar:
        eta = np.array([eta], np.float64)
    if dkScalar:
        dk = np.array([dk], np.float64)
    if thetaScalar:
        theta = np.array([theta], np.float64)

    par = [dk, eta, theta]

#   definition of xi:
    eta1=sg*(eta-d)
    noOF = eta1 < 5e1
    xi = eta - d
    xi[noOF] = np.log(1.0+np.exp(eta1[noOF]))/sg
    # xi = np.piecewise(eta-d, [noOF, ~noOF], [lambda etaMinusD: np.log(1.0+np.exp(sg*etaMinusD))/sg,     lambda etaMinusD: etaMinusD])
    # xi = np.where(eta1 < 5e1,      np.log(1.0+np.exp(eta1))/sg,     eta-d )

    xi2=xi*xi

#   definition of the x_i:
    x1=(a1  +b1*xi+c1*   xi2) / (1.0+c1*xi)

    x2=(a2  +b2*xi+c2*d2*xi2) / (1.0+e2*xi+c2*   xi2)

    x3=(a3  +b3*xi+c3*d3*xi2) / (1.0+e3*xi+c3*   xi2)


#   breakpoints:
    s1=x1-x2
    s2=x1
    s3=x1+x3
    s12=np.sqrt(s1)

#   quadrature integrations: 

# 9 significant figure accuracy:  dqleg010
# 14 significant figure accuracy: dqleg020
# 18 significant figure accuracy: dqleg040
# 32 significant figure accuracy: dqleg080
    dqleg = dqleg040
    dqlag = dqlag040

    if calcDerivatives:
        res1, dres1, ddres1 = dqleg(fdfunc2,  0.0,  s12, par,True)
        res2, dres2, ddres2 = dqleg(fdfunc1,   s1,   s2, par,True)
        res3, dres3, ddres3 = dqleg(fdfunc1,   s2,   s3, par,True)
        res4, dres4, ddres4 = dqlag(fdfunc1,   s3,  1.0, par,True)
        
        # sum the contributions
        fd      = res1 + res2 + res3 + res4
        fdeta   = dres1 + dres2 + dres3 + dres4
        fdtheta = ddres1 + ddres2 + ddres3 + ddres4

        if scalar:
            return fd[0], fdeta[0], fdtheta[0]
        else:
            return fd, fdeta, fdtheta

    else:
        res1 = dqleg(fdfunc2,  0.0,  s12, par,False)
        res2 = dqleg(fdfunc1,   s1,   s2, par,False)
        res3 = dqleg(fdfunc1,   s2,   s3, par,False)
        res4 = dqlag(fdfunc1,   s3,  1.0, par,False)
        
        # sum the contributions
        fd      = res1 + res2 + res3 + res4

        if scalar:
            return fd[0]
        else:
            return fd




def  fdfunc1(x,par,calcDerivatives):
      
    """
        forms the fermi-dirac integrand and its derivatives with eta and theta.
        on input x is the integration variable, par[0] is the double precision 
        index, par[1] is the degeneracy parameter, and par[2] is the relativity
        parameter. on output fd is the integrand, fdeta is the derivative
        with respect to eta, and fdtheta is the derivative with respect to theta.
    """

# initialize
    dk    = par[0]
    eta   = par[1]
    theta = par[2]
    xdk   = x**dk
    dxst  = np.sqrt(1.0 + 0.5*x*theta)

#   avoid overflow in the exponentials at large x
    xMinusEta = x - eta
    noOf = xMinusEta < 1e2
    of = ~noOf
    
    if not np.any(of):# this is for performance (slicing is faster than bool indexing)
        
        of = slice(0, 0)# meaning none
        noOf = slice(None)# meaning all

    factorNoOf = np.exp(xMinusEta[noOf])
    factorOf = np.exp(-xMinusEta[of])
    denomNoOf = factorNoOf + 1.0
    
    fd = xdk * dxst
    fd[noOf] /= denomNoOf
    fd[of] *= factorOf

    if calcDerivatives:
        # Here we want to calc the derivatives
        
        # d eta
        fdeta = fd.copy()
        # fdeta[OF] *= 1 
        fdeta[noOf] *= factorNoOf/denomNoOf
        
        # d theta
        denom2NoOf = 4.0 * dxst[noOf] * denomNoOf
        denom2Of = 4.0 * dxst[of]

        fdtheta = x * xdk
        fdtheta[noOf] /= denom2NoOf
        fdtheta[of] *= factorOf / denom2Of

        return fd, fdeta, fdtheta
    else:
        return fd





def  fdfunc2(x,par,calcDerivatives):
      
    """
        forms the fermi-dirac integrand and its derivatives with eta and theta,
        when the z**2=x variable change has been made.
        on input x is the integration variable, par[0] is the double precision 
        index, par[1] is the degeneravy parameter, and par[2] is the relativity
        parameter. on output fd is the integrand, fdeta is the derivative
        with respect to eta, and fdtheta is the derivative with respect to theta.
    """

    dk    = par[0]
    eta   = par[1]
    theta = par[2]
    xsq   = x * x
    xdk   = x**(2.0e0 * dk + 1.0e0)
    dxst  = np.sqrt(1.0e0 + 0.5e0 * xsq * theta)

#   avoid an overflow in the denominator at large x:
    xsqMinusEta = xsq - eta
    noOf = xsqMinusEta < 1e2
    noUf = xsqMinusEta > -1e2
    noOf_or_Uf = noOf & noUf
    of = ~noOf
    if not np.any(of):# this is for performance (slicing is faster than bool indexing)
        of = slice(0, 0)
        noOf = slice(None)
    elif not np.any(noOf):
        noOf = slice(0,0)
        of = slice(None)
        noOf_or_Uf = slice(0,0)

    factorNoOf = np.ones_like(xsqMinusEta)
    factorNoOf[noOf] = 0
    factorNoOf[noOf_or_Uf] = np.exp(xsqMinusEta[noOf_or_Uf])
    factorNoOf = factorNoOf[noOf]
    factorOf = np.exp(-xsqMinusEta[of])
    denomNoOf = factorNoOf + 1.0
    
    fd = 2.0 * xdk * dxst
    fd[noOf] /= denomNoOf
    fd[of] *= factorOf

    if calcDerivatives:
        # Here we want to calc the derivatives
        
        # d eta
        fdeta = fd.copy()
        # fdeta[OF] *= 1 
        fdeta[noOf] *= factorNoOf/denomNoOf
        
        # d theta
        denom2NoOf = 4.0 * dxst[noOf] * denomNoOf
        denom2Of = 4.0 * dxst[of]

        fdtheta = 2.0e0 * xsq * xdk
        fdtheta[of] *= factorOf / denom2Of
        fdtheta[noOf] /= denom2NoOf

        return fd, fdeta, fdtheta
    else:
        return fd
      





def  dqleg010(f,a,b,par,calcDerivatives):

    """
        10 point gauss-legendre rule for the fermi-dirac function and
        its derivatives with respect to eta and theta.
        on input f is the name of the subroutine containing the integrand,
        a is the lower end point of the interval, b is the higher end point,
        par is an array of constant parameters to be passed to subroutine f,
        and n is the length of the par array. on output result is the 
        approximation from applying the 10-point gauss-legendre rule,
        dresult is the derivative with respect to eta, and ddresult is the
        derivative with respect to theta.
        # 
        note: since the number of nodes is even, zero is not an abscissa.

        the abscissae and weights are given for the interval (-1,1).
        xg     - abscissae of the 20-point gauss-legendre rule
                for half of the usual run (-1,1), i.e.
                the positive nodes of the 20-point rule
        wg     - weights of the 20-point gauss rule.
        c abscissae and weights were evaluated with 100 decimal digit arithmetic.
    """  

    xg = np.array([
            1.48874338981631210884826001129719984e-1,
            4.33395394129247190799265943165784162e-1,
            6.79409568299024406234327365114873575e-1,
            8.65063366688984510732096688423493048e-1,
            9.73906528517171720077964012084452053e-1])

    wg = np.array([
            2.95524224714752870173892994651338329e-1,
            2.69266719309996355091226921569469352e-1,
            2.19086362515982043995534934228163192e-1,
            1.49451349150580593145776339657697332e-1,
            6.66713443086881375935688098933317928e-2])


#           list of major variables
#           -----------------------
# c           absc   - abscissa
#           fval*  - function value
#           result - result of the 10-point gauss formula

    center   = 0.5e0 * (a+b)
    hlfrun   = 0.5e0 * (b-a)
    result   = 0.0e0

    if calcDerivatives:
        dresult  = 0.0e0
        ddresult = 0.0e0
        for j in range(0,5):
            absc1 = center + hlfrun*xg[j]
            absc2 = center - hlfrun*xg[j]
            fval1, dfval1, ddfval1 = f(absc1, par, True)
            fval2, dfval2, ddfval2 = f(absc2, par, True)
            result   = result + (fval1 + fval2)*wg[j]
            dresult  = dresult + (dfval1 + dfval2)*wg[j]
            ddresult = ddresult + (ddfval1 + ddfval2)*wg[j]
        
        result   = result * hlfrun
        dresult  = dresult * hlfrun
        ddresult = ddresult * hlfrun
        return result, dresult, ddresult
    
    else:
        for j in range(0,5):
            absc1 = center + hlfrun*xg[j]
            absc2 = center - hlfrun*xg[j]
            fval1 = f(absc1, par, False)
            fval2 = f(absc2, par, False)
            result   = result + (fval1 + fval2)*wg[j]
        
        result   = result * hlfrun
        return result
    




def  dqleg020(f,a,b,par,calcDerivatives):

    """
        20 point gauss-legendre rule for the fermi-dirac function and
        its derivatives with respect to eta and theta.
        on input f is the name of the subroutine containing the integrand,
        a is the lower end point of the interval, b is the higher end point,
        par is an array of constant parameters to be passed to subroutine f,
        and n is the length of the par array. on output result is the 
        approximation from applying the 20-point gauss-legendre rule,
        dresult is the derivative with respect to eta, and ddresult is the
        derivative with respect to theta.
        
        note: since the number of nodes is even, zero is not an abscissa.


        the abscissae and weights are given for the interval (-1,1).
        xg     - abscissae of the 20-point gauss-legendre rule
                 for half of the usual run (-1,1), i.e.
                 the positive nodes of the 20-point rule
        wg     - weights of the 20-point gauss rule.
        c abscissae and weights were evaluated with 100 decimal digit arithmetic.
    """

    xg = np.array([
            7.65265211334973337546404093988382110e-2,
            2.27785851141645078080496195368574624e-1,
            3.73706088715419560672548177024927237e-1,
            5.10867001950827098004364050955250998e-1,
            6.36053680726515025452836696226285936e-1,
            7.46331906460150792614305070355641590e-1,
            8.39116971822218823394529061701520685e-1,
            9.12234428251325905867752441203298113e-1,
            9.63971927277913791267666131197277221e-1,
            9.93128599185094924786122388471320278e-1])
    wg = np.array([
            1.52753387130725850698084331955097593e-1,
            1.49172986472603746787828737001969436e-1,
            1.42096109318382051329298325067164933e-1,
            1.31688638449176626898494499748163134e-1,
            1.18194531961518417312377377711382287e-1,
            1.01930119817240435036750135480349876e-1,
            8.32767415767047487247581432220462061e-2,
            6.26720483341090635695065351870416063e-2,
            4.06014298003869413310399522749321098e-2,
            1.76140071391521183118619623518528163e-2])


#           list of major variables
#           -----------------------
# c           absc   - abscissa
#           fval*  - function value
#           result - result of the 20-point gauss formula


    center   = 0.5e0 * (a+b)
    hlfrun   = 0.5e0 * (b-a)
    result   = 0.0e0

    if calcDerivatives:
        dresult  = 0.0e0
        ddresult = 0.0e0
        for j  in range(0,10):
            absc1 = center + hlfrun*xg[j]
            absc2 = center - hlfrun*xg[j]
            fval1, dfval1, ddfval1 = f(absc1, par, True)
            fval2, dfval2, ddfval2 = f(absc2, par, True)
            result   = result + (fval1 + fval2)*wg[j]
            dresult  = dresult + (dfval1 + dfval2)*wg[j]
            ddresult = ddresult + (ddfval1 + ddfval2)*wg[j]
        
        result   = result * hlfrun
        dresult  = dresult * hlfrun
        ddresult = ddresult * hlfrun
        return result, dresult, ddresult

    else:
        for j  in range(0,10):
            absc1 = center + hlfrun*xg[j]
            absc2 = center - hlfrun*xg[j]
            fval1 = f(absc1, par, False)
            fval2 = f(absc2, par, False)
            result   = result + (fval1 + fval2)*wg[j]
        
        result   = result * hlfrun
        return result
    





def  dqleg040(f,a,b,par,calcDerivatives):

    """
        40 point gauss-legendre rule for the fermi-dirac function and
        its derivatives with respect to eta and theta.
        on input f is the name of the subroutine containing the integrand,
        a is the lower end point of the interval, b is the higher end point,
        par is an array of constant parameters to be passed to subroutine f,
        and n is the length of the par array. on output result is the 
        approximation from applying the 40-point gauss-legendre rule,
        dresult is the derivative with respect to eta, and ddresult is the
        derivative with respect to theta.
        
        note: since the number of nodes is even, zero is not an abscissa.


        the abscissae and weights are given for the interval (-1,1).
        xg     - abscissae of the 40-point gauss-legendre rule
                 for half of the usual run (-1,1), i.e.
                 the positive nodes of the 40-point rule
        wg     - weights of the 40-point gauss rule.
        c abscissae and weights were evaluated with 100 decimal digit arithmetic.
    """  

    xg = np.array([
            3.87724175060508219331934440246232946e-2,
            1.16084070675255208483451284408024113e-1,
            1.92697580701371099715516852065149894e-1,
            2.68152185007253681141184344808596183e-1,
            3.41994090825758473007492481179194310e-1,
            4.13779204371605001524879745803713682e-1,
            4.83075801686178712908566574244823004e-1,
            5.49467125095128202075931305529517970e-1,
            6.12553889667980237952612450230694877e-1,
            6.71956684614179548379354514961494109e-1,
            7.27318255189927103280996451754930548e-1,
            7.78305651426519387694971545506494848e-1,
            8.24612230833311663196320230666098773e-1,
            8.65959503212259503820781808354619963e-1,
            9.02098806968874296728253330868493103e-1,
            9.32812808278676533360852166845205716e-1,
            9.57916819213791655804540999452759285e-1,
            9.77259949983774262663370283712903806e-1,
            9.90726238699457006453054352221372154e-1,
            9.98237709710559200349622702420586492e-1])
    wg = np.array([
            7.75059479784248112637239629583263269e-2,
            7.70398181642479655883075342838102485e-2,
            7.61103619006262423715580759224948230e-2,
            7.47231690579682642001893362613246731e-2,
            7.28865823958040590605106834425178358e-2,
            7.06116473912867796954836308552868323e-2,
            6.79120458152339038256901082319239859e-2,
            6.48040134566010380745545295667527300e-2,
            6.13062424929289391665379964083985959e-2,
            5.74397690993915513666177309104259856e-2,
            5.32278469839368243549964797722605045e-2,
            4.86958076350722320614341604481463880e-2,
            4.38709081856732719916746860417154958e-2,
            3.87821679744720176399720312904461622e-2,
            3.34601952825478473926781830864108489e-2,
            2.79370069800234010984891575077210773e-2,
            2.22458491941669572615043241842085732e-2,
            1.64210583819078887128634848823639272e-2,
            1.04982845311528136147421710672796523e-2,
            4.52127709853319125847173287818533272e-3])


#           list of major variables
#           -----------------------
# c           absc   - abscissa
#           fval*  - function value
#           result - result of the 20-point gauss formula

    center   = 0.5e0 * (a+b)
    hlfrun   = 0.5e0 * (b-a)
    result   = 0.0e0
    
    if calcDerivatives:
        dresult  = 0.0e0
        ddresult = 0.0e0
        for j in range(0,20):
            absc1 = center + hlfrun*xg[j]
            absc2 = center - hlfrun*xg[j]
            fval1, dfval1, ddfval1 = f(absc1, par, True)
            fval2, dfval2, ddfval2 = f(absc2, par, True)
            result   = result + (fval1 + fval2)*wg[j]
            dresult  = dresult + (dfval1 + dfval2)*wg[j]
            ddresult = ddresult + (ddfval1 + ddfval2)*wg[j]
        
        result   = result * hlfrun
        dresult  = dresult * hlfrun
        ddresult = ddresult * hlfrun
        return result, dresult, ddresult
    else:
        for j in range(0,20):
            absc1 = center + hlfrun*xg[j]
            absc2 = center - hlfrun*xg[j]
            fval1 = f(absc1, par, False)
            fval2 = f(absc2, par, False)
            result   = result + (fval1 + fval2)*wg[j]
        
        result   = result * hlfrun
        return result

    





def  dqleg080(f,a,b,par,calcDerivatives):
      
    """
        80 point gauss-legendre rule for the fermi-dirac function and
        its derivatives with respect to eta and theta.
        on input f is the name of the subroutine containing the integrand,
        a is the lower end point of the interval, b is the higher end point,
        par is an array of constant parameters to be passed to subroutine f,
        and n is the length of the par array. on output result is the 
        approximation from applying the 80-point gauss-legendre rule,
        dresult is the derivative with respect to eta, and ddresult is the
        derivative with respect to theta.
        
        note: since the number of nodes is even, zero is not an abscissa.


        the abscissae and weights are given for the interval (-1,1).
        xg     - abscissae of the 80-point gauss-legendre rule
                 for half of the usual run (-1,1), i.e.
                 the positive nodes of the 80-point rule
        wg     - weights of the 80-point gauss rule.
        c abscissae and weights were evaluated with 100 decimal digit arithmetic.
    """

    xg = np.array([
            1.95113832567939976543512341074545479e-2,
            5.85044371524206686289933218834177944e-2,
            9.74083984415845990632784501049369020e-2,
            1.36164022809143886559241078000717067e-1,
            1.74712291832646812559339048011286195e-1,
            2.12994502857666132572388538666321823e-1,
            2.50952358392272120493158816035004797e-1,
            2.88528054884511853109139301434713898e-1,
            3.25664370747701914619112943627358695e-1,
            3.62304753499487315619043286358963588e-1,
            3.98393405881969227024379642517533757e-1,
            4.33875370831756093062386700363181958e-1,
            4.68696615170544477036078364935808657e-1,
            5.02804111888784987593672750367568003e-1,
            5.36145920897131932019857253125400904e-1,
            5.68671268122709784725485786624827158e-1,
            6.00330622829751743154746299164006848e-1,
            6.31075773046871966247928387289336863e-1,
            6.60859898986119801735967122844317234e-1,
            6.89637644342027600771207612438935266e-1,
            7.17365185362099880254068258293815278e-1,
            7.44000297583597272316540527930913673e-1,
            7.69502420135041373865616068749026083e-1,
            7.93832717504605449948639311738454358e-1,
            8.16954138681463470371124994012295707e-1,
            8.38831473580255275616623043902867064e-1,
            8.59431406663111096977192123491656492e-1,
            8.78722567678213828703773343639124407e-1,
            8.96675579438770683194324071967395986e-1,
            9.13263102571757654164733656150947478e-1,
            9.28459877172445795953045959075453133e-1,
            9.42242761309872674752266004500001735e-1,
            9.54590766343634905493481517021029508e-1,
            9.65485089043799251452273155671454998e-1,
            9.74909140585727793385645230069136276e-1,
            9.82848572738629070418288027709116473e-1,
            9.89291302499755531026503167136631385e-1,
            9.94227540965688277892063503664911698e-1,
            9.97649864398237688899494208183122985e-1,
            9.99553822651630629880080499094567184e-1])
    wg = np.array([
            3.90178136563066548112804392527540483e-2,
            3.89583959627695311986255247722608223e-2,
            3.88396510590519689317741826687871658e-2,
            3.86617597740764633270771102671566912e-2,
            3.84249930069594231852124363294901384e-2,
            3.81297113144776383442067915657362019e-2,
            3.77763643620013974897749764263210547e-2,
            3.73654902387304900267053770578386691e-2,
            3.68977146382760088391509965734052192e-2,
            3.63737499058359780439649910465228136e-2,
            3.57943939534160546028615888161544542e-2,
            3.51605290447475934955265923886968812e-2,
            3.44731204517539287943642267310298320e-2,
            3.37332149846115228166751630642387284e-2,
            3.29419393976454013828361809019595361e-2,
            3.21004986734877731480564902872506960e-2,
            3.12101741881147016424428667206035518e-2,
            3.02723217595579806612200100909011747e-2,
            2.92883695832678476927675860195791396e-2,
            2.82598160572768623967531979650145302e-2,
            2.71882275004863806744187066805442598e-2,
            2.60752357675651179029687436002692871e-2,
            2.49225357641154911051178470032198023e-2,
            2.37318828659301012931925246135684162e-2,
            2.25050902463324619262215896861687390e-2,
            2.12440261157820063887107372506131285e-2,
            1.99506108781419989288919287151135633e-2,
            1.86268142082990314287354141521572090e-2,
            1.72746520562693063585842071312909998e-2,
            1.58961835837256880449029092291785257e-2,
            1.44935080405090761169620745834605500e-2,
            1.30687615924013392937868258970563403e-2,
            1.16241141207978269164667699954326348e-2,
            1.01617660411030645208318503524069436e-2,
            8.68394526926085842640945220403428135e-3,
            7.19290476811731275267557086795650747e-3,
            5.69092245140319864926910711716201847e-3,
            4.18031312469489523673930420168135132e-3,
            2.66353358951268166929353583166845546e-3,
            1.14495000318694153454417194131563611e-3])


#           list of major variables
#           -----------------------
# c           absc   - abscissa
#           fval*  - function value
#           result - result of the 20-point gauss formula


    center   = 0.5e0 * (a+b)
    hlfrun   = 0.5e0 * (b-a)
    result   = 0.0e0

    if calcDerivatives:
        dresult  = 0.0e0
        ddresult = 0.0e0
        for j in range(0, 40):
            absc1 = center + hlfrun*xg[j]
            absc2 = center - hlfrun*xg[j]
            fval1, dfval1, ddfval1 = f(absc1, par, True)
            fval2, dfval2, ddfval2 = f(absc2, par, True)
            result   = result + (fval1 + fval2)*wg[j]
            dresult  = dresult + (dfval1 + dfval2)*wg[j]
            ddresult = ddresult + (ddfval1 + ddfval2)*wg[j]
    
        result   = result * hlfrun
        dresult  = dresult * hlfrun
        ddresult = ddresult * hlfrun
        return result, dresult, ddresult

    else:
        for j in range(0, 40):
            absc1 = center + hlfrun*xg[j]
            absc2 = center - hlfrun*xg[j]
            fval1 = f(absc1, par, False)
            fval2 = f(absc2, par, False)
            result   = result + (fval1 + fval2)*wg[j]
    
        result   = result * hlfrun
        return result

    





def  dqlag010(f,a,b,par,calcDerivatives):
      
    """
        10 point gauss-laguerre rule for the fermi-dirac function.
        on input f is the external function defining the integrand
        f(x)=g(x)*w(x), where w(x) is the gaussian weight 
        w(x)=exp(-(x-a)/b) and g(x) a smooth function, 
        a is the lower end point of the interval, b is the higher end point,
        par is an array of constant parameters to be passed to the function f,
        and n is the length of the par array. on output result is the 
        approximation from applying the 10-point gauss-laguerre rule. 
        since the number of nodes is even, zero is not an abscissa.


        the abscissae and weights are given for the interval (0,+inf).
        xg     - abscissae of the 10-point gauss-laguerre rule
        wg     - weights of the 10-point gauss rule. since f yet
                 includes the weight function, the values in wg 
                 are actually exp(xg) times the standard
                 gauss-laguerre weights
        c abscissae and weights were evaluated with 100 decimal digit arithmetic.
    """

    xg = np.array([
            1.37793470540492430830772505652711188e-1,
            7.29454549503170498160373121676078781e-1,
            1.80834290174031604823292007575060883e0,
            3.40143369785489951448253222140839067e0,
            5.55249614006380363241755848686876285e0,
            8.33015274676449670023876719727452218e0,
            1.18437858379000655649185389191416139e1,
            1.62792578313781020995326539358336223e1,
            2.19965858119807619512770901955944939e1,
            2.99206970122738915599087933407991951e1])
    wg = np.array([
            3.54009738606996308762226891442067608e-1,
            8.31902301043580738109829658127849577e-1,
            1.33028856174932817875279219439399369e0,
            1.86306390311113098976398873548246693e0,
            2.45025555808301016607269373165752256e0,
            3.12276415513518249615081826331455472e0,
            3.93415269556152109865581245924823077e0,
            4.99241487219302310201148565243315445e0,
            6.57220248513080297518766871037611234e0,
            9.78469584037463069477008663871859813e0])


#           list of major variables
#           -----------------------
#           absc   - abscissa
#           fval*  - function value
#           result - result of the 10-point gauss formula

    result   = 0.0e0
    if calcDerivatives:
        dresult  = 0.0e0
        ddresult = 0.0e0
        for j in range(0, 10):
            absc = a+b*xg[j]
            fval, dfval, ddfval = f(absc, par, True)
            result   = result + fval*wg[j]
            dresult  = dresult + dfval*wg[j]
            ddresult = ddresult + ddfval*wg[j]
        
        result   = result*b
        dresult  = dresult*b
        ddresult = ddresult*b
        return result, dresult, ddresult
    else:
        for j in range(0, 10):
            absc = a+b*xg[j]
            fval = f(absc, par, False)
            result   = result + fval*wg[j]
        
        result   = result*b
        return result
    





def  dqlag020(f,a,b,par,calcDerivatives):
      
    """
        20 point gauss-laguerre rule for the fermi-dirac function.
        on input f is the external function defining the integrand
        f(x)=g(x)*w(x), where w(x) is the gaussian weight 
        w(x)=dexp(-(x-a)/b) and g(x) a smooth function, 
        a is the lower end point of the interval, b is the higher end point,
        par is an array of constant parameters to be passed to the function f,
        and n is the length of the par array. on output result is the 
        approximation from applying the 20-point gauss-laguerre rule. 
        since the number of nodes is even, zero is not an abscissa.


        the abscissae and weights are given for the interval (0,+inf).
        xg     - abscissae of the 20-point gauss-laguerre rule
        wg     - weights of the 20-point gauss rule. since f yet
                 includes the weight function, the values in wg 
                 are actually exp(xg) times the standard
                 gauss-laguerre weights
        c abscissae and weights were evaluated with 100 decimal digit arithmetic.
    """
    xg = np.array([
            7.05398896919887533666890045842150958e-2,
            3.72126818001611443794241388761146636e-1,
            9.16582102483273564667716277074183187e-1,
            1.70730653102834388068768966741305070e0,
            2.74919925530943212964503046049481338e0,
            4.04892531385088692237495336913333219e0,
            5.61517497086161651410453988565189234e0,
            7.45901745367106330976886021837181759e0,
            9.59439286958109677247367273428279837e0,
            1.20388025469643163096234092988655158e1,
            1.48142934426307399785126797100479756e1,
            1.79488955205193760173657909926125096e1,
            2.14787882402850109757351703695946692e1,
            2.54517027931869055035186774846415418e1,
            2.99325546317006120067136561351658232e1,
            3.50134342404790000062849359066881395e1,
            4.08330570567285710620295677078075526e1,
            4.76199940473465021399416271528511211e1,
            5.58107957500638988907507734444972356e1,
            6.65244165256157538186403187914606659e1])
    wg = np.array([
            1.81080062418989255451675405913110644e-1,
            4.22556767878563974520344172566458197e-1,
            6.66909546701848150373482114992515927e-1,
            9.15352372783073672670604684771868067e-1,
            1.16953970719554597380147822239577476e0,
            1.43135498592820598636844994891514331e0,
            1.70298113798502272402533261633206720e0,
            1.98701589079274721410921839275129020e0,
            2.28663578125343078546222854681495651e0,
            2.60583472755383333269498950954033323e0,
            2.94978373421395086600235416827285951e0,
            3.32539578200931955236951937421751118e0,
            3.74225547058981092111707293265377811e0,
            4.21423671025188041986808063782478746e0,
            4.76251846149020929695292197839096371e0,
            5.42172604424557430380308297989981779e0,
            6.25401235693242129289518490300707542e0,
            7.38731438905443455194030019196464791e0,
            9.15132873098747960794348242552950528e0,
            1.28933886459399966710262871287485278e1])


#           list of major variables
#           -----------------------
#           absc   - abscissa
#           fval*  - function value
#           result - result of the 20-point gauss formula

    result   = 0.0e0

    if calcDerivatives:
        dresult  = 0.0e0
        ddresult = 0.0e0
        for j in range(0, 20):
            absc = a+b*xg[j]
            fval, dfval, ddfval = f(absc, par, True)
            result   = result + fval*wg[j]
            dresult  = dresult + dfval*wg[j]
            ddresult = ddresult + ddfval*wg[j]
        
        result   = result*b
        dresult  = dresult*b
        ddresult = ddresult*b
        return result, dresult, ddresult

    else:
        for j in range(0, 20):
            absc = a+b*xg[j]
            fval = f(absc, par, False)
            result   = result + fval*wg[j]
        
        result   = result*b
        return result
    




def  dqlag040(f,a,b,par,calcDerivatives):

    """
        20 point gauss-laguerre rule for the fermi-dirac function.
        on input f is the external function defining the integrand
        f(x)=g(x)*w(x), where w(x) is the gaussian weight 
        w(x)=dexp(-(x-a)/b) and g(x) a smooth function, 
        a is the lower end point of the interval, b is the higher end point,
        par is an array of constant parameters to be passed to the function f,
        and n is the length of the par array. on output result is the 
        approximation from applying the 20-point gauss-laguerre rule. 
        since the number of nodes is even, zero is not an abscissa.


        the abscissae and weights are given for the interval (0,+inf).
        xg     - abscissae of the 20-point gauss-laguerre rule
        wg     - weights of the 20-point gauss rule. since f yet
                 includes the weight function, the values in wg 
                 are actually exp(xg) times the standard
                 gauss-laguerre weights
        c abscissae and weights were evaluated with 100 decimal digit arithmetic.
    """  
    xg = np.array([
            3.57003943088883851220844712866008554e-2,
            1.88162283158698516003589346219095913e-1,
            4.62694281314576453564937524561190364e-1,
            8.59772963972934922257272224688722412e-1,
            1.38001082052733718649800032959526559e0,
            2.02420913592282673344206600280013075e0,
            2.79336935350681645765351448602664039e0,
            3.68870267790827020959152635190868698e0,
            4.71164114655497269361872283627747369e0,
            5.86385087834371811427316423799582987e0,
            7.14724790810228825068569195197942362e0,
            8.56401701758616376271852204208813232e0,
            1.01166340484519394068496296563952448e1,
            1.18078922940045848428415867043606304e1,
            1.36409337125370872283716763606501202e1,
            1.56192858933390738372019636521880145e1,
            1.77469059500956630425738774954243772e1,
            2.00282328345748905296126148101751172e1,
            2.24682499834984183513717862289945366e1,
            2.50725607724262037943960862094009769e1,
            2.78474800091688627207517041404557997e1,
            3.08001457394454627007543851961911114e1,
            3.39386570849137196090988585862819990e1,
            3.72722458804760043283207609906074207e1,
            4.08114928238869204661556755816006426e1,
            4.45686031753344627071230206344983559e1,
            4.85577635330599922809620488067067936e1,
            5.27956111872169329693520211373917638e1,
            5.73018633233936274950337469958921651e1,
            6.21001790727751116121681990578989921e1,
            6.72193709271269987990802775518887054e1,
            7.26951588476124621175219277242619385e1,
            7.85728029115713092805438968334812596e1,
            8.49112311357049845427015647096663186e1,
            9.17898746712363769923371934806273153e1,
            9.93208087174468082501090541654868123e1,
            1.07672440639388272520796767611322664e2,
            1.17122309512690688807650644123550702e2,
            1.28201841988255651192541104389631263e2,
            1.42280044469159997888348835359541764e2])
    wg = np.array([
            9.16254711574598973115116980801374830e-2,
            2.13420584905012080007193367121512341e-1,
            3.35718116680284673880510701616292191e-1,
            4.58540935033497560385432380376452497e-1,
            5.82068165779105168990996365401543283e-1,
            7.06495216367219392989830015673016682e-1,
            8.32026903003485238099112947978349523e-1,
            9.58878198794443111448122679676028906e-1,
            1.08727616203054971575386933317202661e0,
            1.21746232797778097895427785066560948e0,
            1.34969549135676530792393859442394519e0,
            1.48425492977684671120561178612978719e0,
            1.62144416281182197802316884316454527e0,
            1.76159537467676961118424220420981598e0,
            1.90507466589479967668299320597279371e0,
            2.05228834726171671760199582272947454e0,
            2.20369055324509588909828344328140570e0,
            2.35979253852320332354037375378901497e0,
            2.52117414037643299165313690287422820e0,
            2.68849805540884226415950544706374659e0,
            2.86252781321044881203476395983104311e0,
            3.04415066531151710041043967954333670e0,
            3.23440709726353194177490239428867111e0,
            3.43452939842774809220398481891602464e0,
            3.64599282499408907238965646699490434e0,
            3.87058459721651656808475320213444338e0,
            4.11049868043282265583582247263951577e0,
            4.36846872325406347450808338272945025e0,
            4.64795898407446688299303399883883991e0,
            4.95344611240989326218696150785562721e0,
            5.29084840590073657468737365718858968e0,
            5.66820460903297677000730529023263795e0,
            6.09679641474342030593376010859198806e0,
            6.59310886103999953794429664206294899e0,
            7.18249599553689315064429801626699574e0,
            7.90666631138422877369310742310586595e0,
            8.84089249281034652079125595063026792e0,
            1.01408992656211694839094600306940468e1,
            1.22100212992046038985226485875881108e1,
            1.67055206420242974052468774398573553e1])


#           list of major variables
#           -----------------------
#           absc   - abscissa
#           fval*  - function value
#           result - result of the 20-point gauss formula


    result   = 0.0e0

    if calcDerivatives:
        dresult  = 0.0e0
        ddresult = 0.0e0
        for j in range(0, 40):
            absc = a+b*xg[j]
            fval, dfval, ddfval = f(absc, par, True)
            result   = result + fval*wg[j]
            dresult  = dresult + dfval*wg[j]
            ddresult = ddresult + ddfval*wg[j]
        
        result   = result*b
        dresult  = dresult*b
        ddresult = ddresult*b
        return result, dresult, ddresult
    
    else:
        for j in range(0, 40):
            absc = a+b*xg[j]
            fval = f(absc, par, False)
            result   = result + fval*wg[j]
        
        result   = result*b
        return result
    





def  dqlag080(f,a,b,par,calcDerivatives):
      
    """
        20 point gauss-laguerre rule for the fermi-dirac function.
        on input f is the external function defining the integrand
        f(x)=g(x)*w(x), where w(x) is the gaussian weight 
        w(x)=dexp(-(x-a)/b) and g(x) a smooth function, 
        a is the lower end point of the interval, b is the higher end point,
        par is an array of constant parameters to be passed to the function f,
        and n is the length of the par array. on output result is the 
        approximation from applying the 20-point gauss-laguerre rule. 
        since the number of nodes is even, zero is not an abscissa.


        the abscissae and weights are given for the interval (0,+inf).
        xg     - abscissae of the 20-point gauss-laguerre rule
        wg     - weights of the 20-point gauss rule. since f yet
                 includes the weight function, the values in wg 
                 are actually exp(xg) times the standard
                 gauss-laguerre weights
        c abscissae and weights were evaluated with 100 decimal digit arithmetic.
    """
    xg = np.array([
            1.79604233006983655540103192474016803e-2,
            9.46399129943539888113902724652172943e-2,
            2.32622868125867569207706157216349831e-1,
            4.31992547802387480255786172497770411e-1,
            6.92828861352021839905702213635446867e-1,
            1.01523255618947143744625436859935350e0,
            1.39932768784287277414419051430978382e0,
            1.84526230383584513811177117769599966e0,
            2.35320887160926152447244708016140181e0,
            2.92336468655542632483691234259732862e0,
            3.55595231404613405944967308324638370e0,
            4.25122008230987808316485766448577637e0,
            5.00944263362016477243367706818206389e0,
            5.83092153860871901982127113295605083e0,
            6.71598597785131711156550087635199430e0,
            7.66499349489177306073418909047823480e0,
            8.67833082516770109543442255542661083e0,
            9.75641480574293071316550944617366591e0,
            1.08996933712878553774361001021489406e1,
            1.21086466423656999007054848698315593e1,
            1.33837881127786473701629840603833297e1,
            1.47256659435085855393358076261838437e1,
            1.61348643716624665791658545428990907e1,
            1.76120052438144378598635686943586520e1,
            1.91577496842412479221729970205674985e1,
            2.07727999097920960924419379010489579e1,
            2.24579012045404583114095916950877516e1,
            2.42138440689586473771922469392447092e1,
            2.60414665601655866929390053565435682e1,
            2.79416568418594655558233069293692111e1,
            2.99153559649009855011270412115737715e1,
            3.19635609022089207107748887542636533e1,
            3.40873278647261898749834947342860505e1,
            3.62877759287814544588031988436216948e1,
            3.85660910092922104582563052172908535e1,
            4.09235302180312671999095850595544326e1,
            4.33614266517312302957826760468219500e1,
            4.58811946612788863456266489974878378e1,
            4.84843356608331891358737273353563006e1,
            5.11724445446070105959889432334907144e1,
            5.39472167895544471206210278787572430e1,
            5.68104563346362231341248503244102122e1,
            5.97640843421099549427295961277471927e1,
            6.28101489639264772036272917590288682e1,
            6.59508362574560573434640627160792248e1,
            6.91884824202362773741980288648237373e1,
            7.25255875442633453588389652616568450e1,
            7.59648311278641748269449794974796502e1,
            7.95090896290888369620572826259980809e1,
            8.31614564010536896630429506875848705e1,
            8.69252644196156234481165926040448396e1,
            9.08041123009407559518411727820318427e1,
            9.48018942159474332072071889138735302e1,
            9.89228344469405791648019372738036790e1,
            1.03171527508039130233047094167345654e2,
            1.07552984977539906327607890798975954e2,
            1.12072690484128333623930046166211013e2,
            1.16736664673503666318157888130801099e2,
            1.21551542490952625566863895752110813e2,
            1.26524665796515540341570265431653573e2,
            1.31664195252120310870089086308006192e2,
            1.36979246686936973947570637289463788e2,
            1.42480058912161601930826569200455232e2,
            1.48178202455004441818652384836007732e2,
            1.54086842281798697859417425265596259e2,
            1.60221072870095715935268416893010646e2,
            1.66598351934053918744521179733712213e2,
            1.73239071334249503830906503775056999e2,
            1.80167323049032317982430208997701523e2,
            1.87411949676963772390490134588021771e2,
            1.95008022441532991450390479600599643e2,
            2.02998984195074937824807677823714777e2,
            2.11439870494836466691484904695542608e2,
            2.20402368151735739654044206677763168e2,
            2.29983206075680004348410969675844754e2,
            2.40319087055841540417597460479219628e2,
            2.51615879330499611167444939310973194e2,
            2.64213823883199102097696108691435553e2,
            2.78766733046004563652014172530611597e2,
            2.96966511995651345758852859155703581e2])
    wg = np.array([
            4.60931031330609664705251321395510083e-2,
            1.07313007783932752564150320304398860e-1,
            1.68664429547948111794220457782702406e-1,
            2.30088089384940054411257181978193282e-1,
            2.91601302502437964832169318772943752e-1,
            3.53226753575408236352723125805647046e-1,
            4.14988177550940466187197686311280092e-1,
            4.76909792302936241314777025418505661e-1,
            5.39016218474955374499507656522327912e-1,
            6.01332497447190529086765248840739512e-1,
            6.63884136396680571849442240727299214e-1,
            7.26697163614156688973567296249140514e-1,
            7.89798189428428531349793078398788294e-1,
            8.53214471438152298354598162431362968e-1,
            9.16973983833892698590342900031553302e-1,
            9.81105491004005747195060155984218607e-1,
            1.04563862580654218147568445663176029e0,
            1.11060397300025890771124763259729371e0,
            1.17603315841226175056651076519208666e0,
            1.24195894449809359279351761817871338e0,
            1.30841533303134064261188542845954645e0,
            1.37543767574892843813155917093490796e0,
            1.44306279387849270398312417207247308e0,
            1.51132910758830693847655020559917703e0,
            1.58027677653099415830201878723121659e0,
            1.64994785280267874116012042819355036e0,
            1.72038644781283277182004281452290770e0,
            1.79163891476093832891442620527688915e0,
            1.86375404864909708435925709028688162e0,
            1.93678330603070923513925434327841646e0,
            2.01078104701134222912614988175555546e0,
            2.08580480238741046429303978512989079e0,
            2.16191556924159897378316344048827763e0,
            2.23917813882364652373453997447445645e0,
            2.31766146114651854068606048043496370e0,
            2.39743905144001430514117238638849980e0,
            2.47858944444973417756369164455222527e0,
            2.56119670357790455335115509222572643e0,
            2.64535099306968892850463441000367534e0,
            2.73114922289915138861410287131169260e0,
            2.81869577775934171703141873747811157e0,
            2.90810334368223018934550276777492687e0,
            2.99949384839685626832412451829968724e0,
            3.09299953469357468116695108353033660e0,
            3.18876418994712376429365271501623466e0,
            3.28694455975337531998378107012216956e0,
            3.38771197960397652334054908762154571e0,
            3.49125426598732012281732423782764895e0,
            3.59777791769613046096294730174902943e0,
            3.70751069001745708341027155659228179e0,
            3.82070461965311695152029959430467622e0,
            3.93763959771430720676800540657330923e0,
            4.05862761338354481597420116187988679e0,
            4.18401782381424031850607692334503121e0,
            4.31420264929613425820084573217987912e0,
            4.44962515053655906604982820155377774e0,
            4.59078802263617511042959849148929810e0,
            4.73826464598929537394753873505838770e0,
            4.89271277966692168696886936743283567e0,
            5.05489168534039512820572507135175938e0,
            5.22568375594272391089278010166022467e0,
            5.40612213379727909853323512340717863e0,
            5.59742640184041404016553694158980053e0,
            5.80104932137643943530626162455394841e0,
            6.01873893878099108768015151514026344e0,
            6.25262247491437403092934213480091928e0,
            6.50532173517668675787482719663696133e0,
            6.78011521200777294201287347980059368e0,
            7.08117122025414518776174311916759402e0,
            7.41389244615305421421695606226687752e0,
            7.78544154841612700386232740339230532e0,
            8.20557347814596472333905086100917119e0,
            8.68801383996161871469419958058255237e0,
            9.25286973415578523923556506201979918e0,
            9.93114471840215736008370986534009772e0,
            1.07739736414646829405750843522990655e1,
            1.18738912465097447081950887710877400e1,
            1.34228858497264236139734940154089734e1,
            1.59197801616897924449554252200185978e1,
            2.14214542964372259537521036186415127e1])


#           list of major variables
#           -----------------------
#           absc   - abscissa
#           fval*  - function value
#           result - result of the 20-point gauss formula

    result   = 0.0e0
    if calcDerivatives:
        dresult  = 0.0e0
        ddresult = 0.0e0
        for j in range(0, 80):
            absc = a+b*xg[j]
            fval, dfval, ddfval = f(absc, par, True)
            result   = result + fval*wg[j]
            dresult  = dresult + dfval*wg[j]
            ddresult = ddresult + ddfval*wg[j]
        
        result   = result*b
        dresult  = dresult*b
        ddresult = ddresult*b
        return result, dresult, ddresult

    else:
        for j in range(0, 80):
            absc = a+b*xg[j]
            fval = f(absc, par, False)
            result   = result + fval*wg[j]
        
        result   = result*b
        return result
    
def fermi_three_halves_fukushima(x):

    factor=2.e0/5.e0    # = 1/(k+1)

# double precision rational minimax approximation of Fermi-Dirac integral of order k=3/2
#
# Reference: Fukushima, T. (2014, submitted to App. Math. Comp.)
#
# Author: Fukushima, T. <Toshio.Fukushima@nao.ac.jp>
#
#
    # masks for the different ranges
    m1 = ( x < -2.e0 )
    m2 = ( x >= -2.e0 ) & ( x < 0.0e0 )
    m3 = ( x >= 0.0e0 ) & ( x < 2.e0 )
    m4 = ( x >= 2.e0 ) & ( x < 5.e0 )
    m5 = ( x >= 5.e0 ) & ( x < 10.e0 )
    m6 = ( x >= 10.e0 ) & ( x < 20.e0 )
    m7 = ( x >= 20.e0 ) & ( x < 40.e0 )
    m8 = ( x >= 40.e0 )

    fd = np.zeros_like(x)

    # Range 1
    ex=np.exp(x[m1])
    t=ex*7.38905609893065023e0
    fd[m1]=ex*(1.32934038817913702e0 \
    -ex*(1346.14119046566636e0 \
    +t*(199.946876779712712e0 \
    +t*(6.5210149677288048e0 \
    +t*0.0108588591982722183e0 \
    )))/(5728.3481201778541e0 \
    +t*(1132.17837281710987e0 \
    +t*(64.805243148002602e0 \
    +t))))
    
    # Range 2
    s=-0.5e0*x[m2]
    t=1.e0-s
    fd[m2]=(631.667081787115831e0 \
    +t*(504.131655805666135e0 \
    +t*(113.449065431934917e0 \
    +t*(56.0939647772947784e0 \
    +t*(43.3374223200846752e0 \
    +t*(12.8047010597109577e0 \
    +t*(0.219164386586949410e0 \
    +t*(-0.678659552658390139e0 \
    -t*0.126533769309899232e0 \
    ))))))))/(1180.5112183558028e0 \
    +s*(1101.0159189871135e0 \
    +s*(864.4448234404281e0 \
    +s*(392.2018227840790e0 \
    +s*(89.58093202779063e0 \
    +s*(-9.95066218572899e0 \
    +s*(-17.312068771997626e0 \
    +s*(-6.4116162917822773e0 \
    -s))))))))
    
    # Range 3
    t=0.5e0*x[m3]
    fd[m3]=(90122.488639370400e0 \
    +t*(157095.208147064037e0 \
    +t*(166879.962599668589e0 \
    +t*(125708.597728045460e0 \
    +t*(69968.278213181390e0 \
    +t*(29035.3292989055404e0 \
    +t*(8736.4439472398517e0 \
    +t*(1747.16784760309227e0 \
    +t*180.132410666734053e0 \
    ))))))))/(78176.777123671727e0 \
    +t*(-1681.44633240543085e0 \
    +t*(38665.7913035496031e0 \
    +t*(-2527.29685826087874e0 \
    +t*(5062.6683078100048e0 \
    +t*(-553.21165462054589e0 \
    +t*(165.395637981775430e0 \
    +t*(- 18.0295465153725544e0 \
    +t))))))))
    
    # Range 4
    t=0.3333333333333333333e0*(x[m4]-2.e0)
    fd[m4]=(912944.432058014054e0 \
    +t*(3.28217091334054338e6 \
    +t*(5.59250227196369585e6 \
    +t*(5.76136129685687470e6 \
    +t*(3.84331519034749983e6 \
    +t*(1.65284168824947710e6 \
    +t*(423452.676670436605e0 \
    +t*49835.4127241373113e0 \
    )))))))/(164873.145721762182e0 \
    +t*(257442.511191094986e0 \
    +t*(225604.160532840884e0 \
    +t*(99932.1955662320024e0 \
    +t*(24761.0878784286761e0 \
    +t*(1398.26392212830777e0 \
    +t*(-36.4450237523474167e0 \
    +t)))))))

    # Range 5
    t=0.2e0*x[m5]-1.e0
    fd[m5]=(1.88412548327216052e6 \
    +t*(8.08838896259910792e6 \
    +t*(1.56869793001790529e7 \
    +t*(1.79109792599373447e7 \
    +t*(1.31345142328147214e7 \
    +t*(6.29500412046744325e6 \
    +t*(1.89326213154091054e6 \
    +t*(312372.643127575407e0 \
    +t*18814.7420442630170e0 \
    ))))))))/(67768.3347951202583e0 \
    +t*(147635.914444221358e0 \
    +t*(151908.303165069423e0 \
    +t*(86671.1222110642970e0 \
    +t*(27855.9481608626219e0 \
    +t*(3833.22697473114940e0 \
    +t*(98.3384567064269554e0 \
    -t)))))))*0.999999999999999876e0  # correction to remove bias
    
    # Range 6
    t=0.1e0*x[m6]-1.e0
    fd[m6]=(1.59656593348660977e9 \
    +t*(7.32769737561517060e9 \
    +t*(1.42662658588280191e10 \
    +t*(1.51238422045169918e10 \
    +t*(9.27233604548095476e9 \
    +t*(3.18834513406577423e9 \
    +t*(5.36061988605886123e8 \
    +t*3.03619219668246382e7 \
    )))))))/(1.18906980815759995e7 \
    +t*(2.62209219322122975e7 \
    +t*(2.28143701746618729e7 \
    +t*(8.57156701742181874e6 \
    +t*(1.13860063870524239e6 \
    +t*(27091.7884208687379e0 \
    +t*(-275.664733379090447e0 \
    +t)))))))*0.999999999999999829e0  # correction to remove bias
    
    # Range 7
    t=0.05e0*x[m7]-1.e0
    fd[m7]=(2.60437581212904589e8 \
    +t*(1.08771546307370080e9 \
    +t*(1.81531350939088943e9 \
    +t*(1.52833764636304939e9 \
    +t*(6.70684451492750149e8 \
    +t*(1.40870639531414149e8 \
    +t*1.04957900377463854e7 \
    ))))))/(358448.871166784200e0 \
    +t*(611808.419702466190e0 \
    +t*(326307.561591723775e0 \
    +t*(58407.9904827573816e0 \
    +t*(2049.50040323021794e0 \
    +t*(-39.8767861209088081e0 \
    +t))))))*0.999999999999999828e0
    
    # Range 8
    w=1.e0/(x[m8]*x[m8])
    s=1.e0-1600.e0*w
    fd[m8]=x[m8]*x[m8]*np.sqrt(x[m8])*factor*(1.e0 \
    +w*(6.16739021212286242e0 \
    +s*(0.00111530123694574981e0 \
    +s*(-2.79156524536560815e-6 \
    +s*(2.95571462110856359e-8 \
    -s*6.70917556862133933e-10)))))

    return fd
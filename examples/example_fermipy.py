#!/usr/bin/env python3

import fermipy

# print fermipy documentation
print(':: fermipy documentation:\n', fermipy.__doc__)

# print dfermi documentation
print(':: dfermi documentation:\n', fermipy.dfermi.__doc__)

# usage dfermi subroutine
dk = 0.5; eta = -1.4; theta = 2.3
fd, fdeta, fdtheta = fermipy.dfermi(dk, eta, theta)
print(':: F_{}({},{}) = {:}, dF/deta = {:}, dF/dtheta = {:}'.format(dk, eta, theta, fd, fdeta, fdtheta))

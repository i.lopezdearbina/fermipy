#!/usr/bin/env python3

import fermi
import numpy as np
import matplotlib.pyplot as plt

dk = 0.5
eta = np.linspace(-100, 100, 200)
theta = 0.05

fd, fdeta, fdtheta = fermi.dfermi(dk, eta, theta, True)

plt.plot(eta, fd)
plt.show()

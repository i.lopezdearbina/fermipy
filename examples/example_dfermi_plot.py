#!/usr/bin/env python3

import fermipy
import numpy as np
import matplotlib.pyplot as plt

dk = 0.5
eta = np.linspace(-100, 100, 200)
theta = 0.05
fd = []; fdeta = []; fdtheta = []
for i in range(len(eta)):
    f, feta, ftheta = fermipy.dfermi(dk, eta[i], theta)
    fd.append(f)
    fdeta.append(ftheta)
    fdtheta.append(fdtheta)

plt.plot(eta, fd)
plt.show()

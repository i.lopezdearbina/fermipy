# Fermi-Dirac Integrals Python Interface

A Python interface to Fermi-Dirac integrals Fortran subroutines of J.M. Aparicio, Apjs 117, 627 1998

## Process

The original `fermi.f` routine has been sligthtly modified to explicitly state the `intent(in)`s and `intent(out)`s so to help f2py.

Use f2py tool over `fermi.f` as:
```
f2py -c fermi.f -m fermipy
```
The output is the library `fermipy.cpython-35m-x86_64-linux-gnu.so`. Due to the
high dependence of the created library to the compiler, versions, architecture,
etc. it is highly recommended to create the library for every machine.

## Python port

A pure python port has been added: The file fermi.py implements the dfermi subroutine using numpy.
An additional optional parameter `calcDerivatives` has been added. If set to `False` (which is the default), only the integral is calculated and not it's derivatives, which may speed up the calculation.
Also, numpy arrays can be passed to the dfermi function. They have to be able to be broadcast together, then the function will calculate the integral at once for the whole arrays.
For backwards compatibility also scalars may be entered and then scalars will be returned, however internally the function will convert them to length 1 numpy arrays.

## Usage

```
import fermipy

dk = 0.5; eta = -1.4; theta = 2.3
fd, fdeta, fdtheta = fermipy.dfermi(dk, eta, theta)

# Using the pure python port:
import fermi

dk = 1.5; eta = np.arange(-1000, 1000, 10).reshape(-1,1); theta = np.geomspace(0.1, 1000).reshape(1,-1)
fd, fdeta, fdtheta = fermi.dfermi(dk, eta, theta, True)

print("eta.shape = {}, theta.shape = {}, fd.shape = {}".format(eta.shape, theta.shape, fd.shape))
# out:"eta.shape = (200, 1), theta.shape = (1, 50), fd.shape = (200, 50)"
```
